# Descriptions

1. Application starts with MongoDB (DB port - 40000)

2. Application functionality:

    2.1 assign permission for particular user for particular department
    
    2.2 revoke permission for particular user for particular department
    
    2.3 receive all inherited permissions for particular user for particular department
    
3. Example of requests:

    3.1 assign permission (without sub-departments)
   
   
   POST http://localhost:8080/assign
   Content-Type: application/json
    
   {
      "userId" : "b05beb44-b7f9-4ec8-ba41-5d0f5844a4aa",
      "departmentId" : "57633250-8e65-4b2a-b814-5838a1b8d3ff",
      "allowedPermission": "EDIT"
   }

   response
   
   {
     "userId": "b05beb44-b7f9-4ec8-ba41-5d0f5844a4ad",
     "departmentId": "57633250-8e65-4b2a-b814-5838a1b8d3ff"
     "permission": "EDIT"
   }
   
   3.2 revoke permission (pattern - `DELETE http://localhost:8080/{{userId}}/{{departmentId}}`)
   
   DELETE http://localhost:8080/b05beb44-b7f9-4ec8-ba41-5d0f5844a4ad/57633250-8e65-4b2a-b814-5838a1b8d3ff
   
   3.3 get assigned permission by user and department
   
   GET http://localhost:8080/b05beb44-b7f9-4ec8-ba41-5d0f5844a4ad/57633250-8e65-4b2a-b814-5838a1b8d3ff
   
   {
        "userId": "b05beb44-b7f9-4ec8-ba41-5d0f5844a4ad",
        "departmentId": "57633250-8e65-4b2a-b814-5838a1b8d3ff",
        "permissions": ["EDIT", "VIEW"]
    }

4. What was disabled OR not implemented

    4.1 Test `revoke` method from my perspective cannot be tested without DB otherwise it will be testing Mockito
    
    4.2 `getAccess` test disabled coz it stuck for unclear reason