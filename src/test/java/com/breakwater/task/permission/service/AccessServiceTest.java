package com.breakwater.task.permission.service;

import com.breakwater.task.department.dto.DepartmentDTO;
import com.breakwater.task.department.service.DepartmentService;
import com.breakwater.task.permission.dto.AccessResponseDTO;
import com.breakwater.task.permission.dto.GrantPermissionDTO;
import com.breakwater.task.permission.model.Access;
import com.breakwater.task.permission.model.AccessId;
import com.breakwater.task.permission.model.GrantPermission;
import com.breakwater.task.permission.repository.AccessRepository;
import com.breakwater.task.user.dto.UserDTO;
import com.breakwater.task.user.service.UserService;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

class AccessServiceTest {

    private static final UUID USER_ID = UUID.randomUUID();
    private static final UUID DEPARTMENT_ID = UUID.randomUUID();

    @Tested
    private AccessService accessService;

    @Injectable
    private DepartmentService departmentService;
    @Injectable
    private UserService userService;
    @Injectable
    private AccessRepository accessRepository;

    @Test
    void assign() {
        var userDTO = new UserDTO(USER_ID, USER_ID.toString());
        var departmentDTO = new DepartmentDTO(DEPARTMENT_ID, DEPARTMENT_ID.toString(), null, null);

        var expectedAccessResponseDTO = new AccessResponseDTO(USER_ID, DEPARTMENT_ID, Set.of(GrantPermissionDTO.EDIT));

        new Expectations() {{
            userService.findUser(USER_ID);
            result = Mono.just(userDTO);

            departmentService.findDepartment(DEPARTMENT_ID);
            result = Mono.just(departmentDTO);
        }};

        StepVerifier.create(accessService.assign(USER_ID, DEPARTMENT_ID, GrantPermissionDTO.EDIT))
                .expectNext(expectedAccessResponseDTO)
                .verifyComplete();
    }

    @Test
    void getAccess() {
        var departmentDTO = new DepartmentDTO(DEPARTMENT_ID, DEPARTMENT_ID.toString(), null, null);
        var accessIds = new ArrayList<AccessId>();
        var access = new Access(new AccessId(USER_ID, DEPARTMENT_ID), GrantPermission.EDIT);
        var accessDTO = new AccessResponseDTO(USER_ID, DEPARTMENT_ID, Set.of(GrantPermissionDTO.EDIT));

        new Expectations() {{
            departmentService.readDepartmentWithParents(DEPARTMENT_ID);
            result = Mono.just(departmentDTO);

            accessRepository.findAllById(s -> withCapture(accessIds));
            this.result = Flux.just(access);
        }};

        StepVerifier.create(accessService.getAccess(USER_ID, DEPARTMENT_ID))
                .expectNext(accessDTO)
                .verifyComplete();
    }
}
