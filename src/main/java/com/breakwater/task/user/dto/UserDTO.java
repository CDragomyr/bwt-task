package com.breakwater.task.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

@Value
public class UserDTO {

    UUID id;
    String nickname;

    @JsonIgnore
    public boolean isValid() {
        return id != null && StringUtils.isNotBlank(nickname);
    }
}
