package com.breakwater.task.permission.model;

import lombok.Value;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Value
@Embeddable
public class AccessId implements Serializable {

    UUID userId;
    UUID departmentId;
}
