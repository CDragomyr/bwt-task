package com.breakwater.task.permission.model;

public enum GrantPermission {
    EDIT, VIEW, NONE
}
