package com.breakwater.task.permission.model;

import lombok.Value;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.EmbeddedId;

@Value
@Document
public class Access {

    @EmbeddedId
    AccessId id;
    GrantPermission permission;
}
