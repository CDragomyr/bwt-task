package com.breakwater.task.permission.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;

import java.util.Set;
import java.util.UUID;

@Value
public class AccessResponseDTO {

//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    UserDTO user;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    DepartmentDTO department;

    UUID userId;
    UUID departmentId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    Set<GrantPermissionDTO> permissions;

//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    String errorMessage;

//    public static AccessResponseDTO buildResponse(UserDTO user, DepartmentDTO department, GrantPermissionDTO permission) {
//    public static AccessResponseDTO buildResponse(UserDTO user, DepartmentDTO department, GrantPermissionDTO permission, boolean allowSubDepartments) {
//        if (!allowSubDepartments) {
//            department = getDepartmentDTO(department);
//        }
//        return new AccessResponseDTO(user, department, permission, null);
//    }

//    private static DepartmentDTO getDepartmentDTO(DepartmentDTO department) {
//        return new DepartmentDTO(department.getId(), department.getName(), null, null);
//    }

//    public static AccessResponseDTO buildErrorResponse(String errorMessage) {
//        return new AccessResponseDTO(null, null, null, errorMessage);
//    }
}
