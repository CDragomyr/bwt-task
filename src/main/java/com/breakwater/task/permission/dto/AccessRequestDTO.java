package com.breakwater.task.permission.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;

import java.util.UUID;

@Value
public class AccessRequestDTO {

    UUID userId;
    UUID departmentId;

    // assign access for sub-departments
    Boolean allowSubDepartment = false;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    GrantPermissionDTO permission;
}
