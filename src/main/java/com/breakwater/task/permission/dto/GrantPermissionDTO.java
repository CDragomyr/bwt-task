package com.breakwater.task.permission.dto;

public enum GrantPermissionDTO {

    EDIT, VIEW, NONE
}
