package com.breakwater.task.permission.service;

import com.breakwater.task.department.dto.DepartmentDTO;
import com.breakwater.task.department.service.DepartmentService;
import com.breakwater.task.permission.dto.AccessResponseDTO;
import com.breakwater.task.permission.dto.GrantPermissionDTO;
import com.breakwater.task.permission.model.Access;
import com.breakwater.task.permission.model.AccessId;
import com.breakwater.task.permission.model.GrantPermission;
import com.breakwater.task.permission.repository.AccessRepository;
import com.breakwater.task.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccessService {

    private final DepartmentService departmentService;
    private final UserService userService;
    private final AccessRepository accessRepository;

    private static Access buildPermission(UUID userId, UUID departmentId, GrantPermissionDTO permission) {
        var accessId = new AccessId(userId, departmentId);
        return new Access(accessId, GrantPermission.valueOf(permission.name()));
    }

    private static Flux<UUID> collectDepartmentIds(Flux<UUID> departmentIds, DepartmentDTO departmentDTO) {
        if (departmentDTO == null) {
            return departmentIds;
        }

        return collectDepartmentIds(departmentIds.concatWithValues(departmentDTO.getId()), departmentDTO.getParent());
    }

    private static Flux<AccessId> collectAccessIds(UUID userId, DepartmentDTO departmentDTO) {
        var departmentIds = collectDepartmentIds(Flux.just(departmentDTO.getId()), departmentDTO.getParent());
        return departmentIds.flatMap(departmentId -> Flux.just(new AccessId(userId, departmentId)));
    }

    public Mono<AccessResponseDTO> assign(UUID userId, UUID departmentId, GrantPermissionDTO permission) {
        var userDTOMono = userService.findUser(userId);
        var departmentDTOMono = departmentService.findDepartment(departmentId);

        return Flux.combineLatest(userDTOMono, departmentDTOMono, (user, department) -> {
            var access = buildPermission(userId, departmentId, permission);
            accessRepository.insert(access).subscribe();
            return new AccessResponseDTO(userId, departmentId, Set.of(permission));
        }).singleOrEmpty();
    }

    public Mono<Void> revoke(UUID userId, UUID departmentId) {
        var accessId = new AccessId(userId, departmentId);
        return accessRepository.deleteById(accessId);
    }

    public Flux<AccessResponseDTO> getAccess(UUID userId, UUID departmentId) {
        var accessIds = departmentService.readDepartmentWithParents(departmentId)
                .switchIfEmpty(Mono.empty())
                .flatMapMany(departmentDTO -> collectAccessIds(userId, departmentDTO));

        return accessRepository.findAllById(accessIds)
                .switchIfEmpty(obj -> {
                    var accessId = new AccessId(userId, departmentId);
                    Flux.just(new Access(accessId, GrantPermission.NONE));
                })
                .map(access -> GrantPermissionDTO.valueOf(access.getPermission().name()))
                .collectList()
                .flatMapMany(grantPermissionDTOs -> Flux.just(new AccessResponseDTO(userId, departmentId, Set.copyOf(grantPermissionDTOs))));
    }
}
