package com.breakwater.task.permission.controller;

import com.breakwater.task.permission.dto.AccessRequestDTO;
import com.breakwater.task.permission.dto.AccessResponseDTO;
import com.breakwater.task.permission.service.AccessService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@AllArgsConstructor
public class PermissionController {

    private final AccessService accessService;

    @PostMapping
    public Mono<AccessResponseDTO> assign(@RequestBody AccessRequestDTO request) {
        return accessService.assign(request.getUserId(), request.getDepartmentId(), request.getPermission());
    }

    @DeleteMapping("/{userId}/{departmentId}")
    public Mono<Void> revoke(@PathVariable String userId, @PathVariable String departmentId) {
        return accessService.revoke(UUID.fromString(userId), UUID.fromString(departmentId));
    }

    @GetMapping("/{userId}/{departmentId}")
    public Flux<AccessResponseDTO> getAccess(@PathVariable String userId, @PathVariable String departmentId) {
        return accessService.getAccess(UUID.fromString(userId), UUID.fromString(departmentId));
    }
}
