package com.breakwater.task.permission.repository;

import com.breakwater.task.permission.model.Access;
import com.breakwater.task.permission.model.AccessId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessRepository extends ReactiveMongoRepository<Access, AccessId> {
}
