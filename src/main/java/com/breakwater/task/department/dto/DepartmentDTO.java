package com.breakwater.task.department.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.UUID;

@Value
public class DepartmentDTO {

    UUID   id;
    String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    DepartmentDTO       parent;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<DepartmentDTO> children;

    @JsonIgnore
    public boolean isValid() {
        return id != null && StringUtils.isNotBlank(name);
    }
}
